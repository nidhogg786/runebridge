/* Imports */
const BnbApiClient = require('@binance-chain/javascript-sdk').BncClient;
const axios = require('axios');
const app = require('../config/config').app;
const utils = require('./utils');

var bnbClient = {};

/* Start Binance Chain Client */
const binanceConnect = async() => {
    try{
        bnbClient = new BnbApiClient(app.BNB_APIURI)
        bnbClient.chooseNetwork(app.BNB_CHAIN);
        await bnbClient.initChain()
        await bnbClient.setPrivateKey(app.BNB_PRIVKEY)
        utils.winston.info("[INFO] WALLET CONNECTED")
        return bnbClient;
    } catch(err){
        console.log(err)
        utils.winston.info("[INFO] ERROR SETTING PRIVATE KEY")
        return err;
    }
}


/* Transfer tokens on binnace chain */
const transferToken = async(job, txType) =>{
        M = "memo"
        o = job.txFrom || job.out.to;
        a = app.BNB_ASSET
        A = job.payout.amount
        
    try{
        let thisSeq = await getSequence()
        const res = await bnbClient.transfer(app.BNB_ADDRESS, o, A, a, M, thisSeq)
        const hash = res.result[0].hash;
        return hash;
    } catch(err){
        console.log(err)
        utils.logger('RELEASE_FAILED', job.id);
        let error = utils.makeError('RELEASE_FAILED')
        return error;
    }
}


/* Get transactions for a binance chain address */
const getTransactions = async(side) => {
    let url = app.BNB_APIURI +'api/v1/transactions?txAsset='+app.BNB_ASSET+'&txType=TRANSFER&side='+side+'&limit=1000&address='+app.BNB_ADDRESS;
    try {
        let res = await axios.get(url)
        return res;
    } catch(err){
        return err;
    }
}

/* Get sequence of account */
const getSequence = async() =>{
    try{
        let url = app.CHAIN_APIURI + 'api/v1/account/'+app.BNB_ADDRESS+'/sequence';
        let res = await axios.get(url)
        return res.data.sequence;
    } catch(err){
        app.state = false;
        return err
    }
}

/* Check Address */
const checkAddress = (address) =>{
    return bnbClient.checkAddress(address)
}

/* EXPORTS */
exports.transferToken = transferToken;
exports.binanceConnect = binanceConnect;
exports.getTransactions = getTransactions;
exports.checkAddress = checkAddress;