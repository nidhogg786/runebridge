/* Local Imports */
const app = require('../config/config').app;
const winston = require('winston');
const binance = require('./binance')

winston.add(new winston.transports.Console())

const logStrings = {
  PARSE_ERROR:      'BEP2: INCOMING TX IGNORED BECAUSE THE OBJECT COULD NOT BE PARSED. HASH: {1}',
  MEMO_EMPTY:       'BEP2: INCOMING TX IGNORED DUE TO EMPTY MEMO. HASH: {1}',
  MEMO_BAD:         'BEP2: INCOMING TX IGNORED DUE TO BAD MEMO INSTRUCTION. HASH: {1}',
  ADDRESS_BAD:     'ECSROW: INCOMING TX IGNORED DUE TO BAD ADDRESS. HASH {1}',
  JOB_DUPLICATE: 'BEP2: CREATE JOB ABORTED. REASON: DUPLICATE JOB. HASH: {1}',
  JOB_SUCCESS:   'BEP2: SUCCESSFULLY CREATED JOB: {1}',
  ASSET_INVALID:  'BEP2: INVALID ASSET',
  ASSET_INSUFFICIENT:  'BEP2: INSUFFICIENT ASSET',
  RELEASE_NOJOB:    'BEP2: RELEASE ABORTED. REASON: JOB NOT FOUND. HASH: {1}',
  RELEASE_DUPLICATE:'BEP2: RELEASE ABORTED. REASON: ALREADY RELEASED. HASH: {1}',
  RELEASE_NOTAUTH:  'BEP2: RELEASE ABORTED. REASON: NOT AUTHORISED. JOB: {1}',
  RELEASE_NOPRICE:  'BEP2: RELEASE ABORTED. REASON: CANT GET PRICE DATA. JOB: {1}',
  RELEASE_SUCCESS:  'BEP2: RELEASE SUCCESSFUL FOR JOB: {1}',
  RELEASE_FAILED:   'BEP2: RELEASE FAILED FOR JOB: {1}',
  RELEASE_NOVALUE:  'BEP2: RELEASE ABORTED. REASON: JOB DOESNT HAVE A PRICE. HASH: {1}',
  REFUND_FAILED:    'BEP2: REFUND FAILED FOR HASH: {1}',
  REFUND_SUCCESS:   'BEP2: REFUND SUCCESS FOR HASH: {1}',
  ERROR_TRANSFER:   'ERROR IN BINANCE TRANSFER',
  PENDING_TX:       'BEP2: INCOMING TX QUEUED DUE TO STATE OFF. HASH: {1}',
  ERROR_SEQ:        'ERROR GETTING SEQUENCE NUMBER',
  ERROR_GECKO:      'ERROR GETTING MARKET DATA FROM COINGECKO',
  ERROR_TX:         'ERROR GETTING TRANSACTION DATA FROM BINANCE API',
  BACKLOG_START:    'SYNCING: CLEARING BACKLOG...',
  BACKLOG_END:      'SYNCING: BACKLOCK CLEARED! ONLINE.',
  REFUND_NOTAUTH:   'REFUND: NOT AUTH. HASH: {1}',
  REFUND_AUTH:      'REFUND: REFUND REQUESTED. HASH {1}',
  REFUND_BADJOB:    'REFUND: REFUND IGNORED. JOB NOT FOUND. HASH {1}',
  REFUND_BADSTATUS: 'REFUND: REFUND IGNORED. JOB NOT ACTIVE. HASH {1}',
  REFUND_ERROR:     'REFUND: REFUND IGNORED. NOT AUTHED, BAD STATUS OR NO JOB. HASH {1}',
}


/* Logging helper */
const logger = (err, data) => {
  var string = logStrings[err] || logStrings[err.message] || 'UNHANDLED ERROR!\n'+err;
  return syncing()
  ? winston.info('[REPLAY] '+ string.replace('{1}',data))
  : err.message !== 'STREAM_BAD'
    ? winston.info(string.replace('{1}',data))
    : false;
}


/* Error helper */
const makeError = (message) => {
  throw new Error(message);
}


/* Update rate limits helper */
const updateRateLimits = (min,sec) => {
  app.binanceRateLimSecond = sec || 0 ;
  app.binanceRateLimMin = min || 0;
  let rateLimStats = 'RATE LIMIT REMAINING: [MIN]:'+app.binanceRateLimMin+' [SEC]:'+app.binanceRateLimSecond;
}

const rateLimitCheck = async() =>{
  await new Promise(resolve => {
    let sec = app.binanceRateLimSecond;
    let min = app.binanceRateLimMin;
    let ms = min < 10 || sec <2 ? 1000 : 100;
    setTimeout(resolve, ms)
  });
}

/* stall helper  */
const stall = async(ms=500) => {
  await new Promise(resolve => setTimeout(resolve, ms));
}


 /**
 * Splits binanace chain memo by colon separator
 * @param   {Object} msg    Telegram Message Object
 * @return  {Object}        Object containing text, command & arguments
 */
  function splitMsg(msg){
      try{
      const text = msg;
      const match = text.match(/^([^:]+):?(.+)?/)
      let args = []
      let command
      if (match !== null) {
        if (match[1]) {
          command = match[1].toUpperCase()
        }
        if (match[2]) {
          args = match[2].split(':')
        }
      }

      let msgObj = {
        raw: text,
        command,
        args
      }

      return msgObj || false;
    } catch(e){
      return false;
    }
  }


/**
* Takes a binance chain payload (or API) & returns a neat object
* @param   {Object}   payload    Binance payload object
* @return  {Object}              Neat object
*/
function parseTransaction(payload){
  let tx = {}
  try{
    tx.txHash   = payload.txHash            || payload.data.H
    tx.txTo     = payload.toAddr            || payload.data.t[0].o
    tx.txFrom   = payload.fromAddr          || payload.data.f
    tx.txTkn    = payload.txAsset           || payload.data.t[0].c[0].a
    tx.txQty    = Number(payload.value)     || Number(payload.data.t[0].c[0].A)
    tx.txMemo   = payload.txHash 
      ? payload.memo
        ? payload.memo
        : null
      : (payload.data.M ? payload.data.M : null)

    tx.txMemo = splitMsg(tx.txMemo)
    return tx
  } catch (e){
    console.log(e)
    return null;
  }
}


/* Build dates helper */
const getPeriods = () => {
  var epochs = {}
  epochs.now = Date.now();
  epochs.msIn28Days = 2419200000;
  epochs.now - epochs.now;

  // Make an array of period objects
  var periods = [];
  const periodsOf28Days = Number(((epochs.now-epochs.genesis)/epochs.msIn28Days).toFixed(0))+1
  for (let i = 0; i < periodsOf28Days; ++i) {
    periods.push({id: i+1,start: (epochs.now-(epochs.msIn28Days*(i+1))+1), end: (epochs.now-(epochs.msIn28Days*i))})
  }

  return periods;
}


/* Sync stats helper */
const getSyncStats = () => {
  var stats = {}
  stats.active = app.jobs.filter(x => x.status == 'ACTIVE').length
  stats.pending = app.jobs.filter(x => x.status == 'PENDING').length
  stats.completed = app.jobs.filter(x => x.status == 'COMPLETED').length
  stats.refunded = app.jobs.filter(x => x.status == 'REFUNDED').length
  return stats;
}

/* Get # of jobs */
const countJobs = () => {
  return app.jobs.length;
}

/**
* Takes a binance chain payload (or API) & checks it can be handled.
* @param   {Object}   payload    Binance payload object
* @return  {Promise}             Promise
*/
const validatePayload = async(payload) => {
    let tx = parseTransaction(payload)                      || makeError('PARSE_ERROR')
    let txIsValid = h.txIsValid(tx)                         || makeError('PARSE_ERROR')
    let txHasMemo = h.txHasMemo(tx)                         || makeError('MEMO_EMPTY')
    let txHasValidMemo = h.txHasValidMemo(tx)               || makeError('MEMO_BAD')
    let txHasMinAsset = h.txHasMinAsset(tx)                 || makeError('ASSET_INSUFFICIENT')
    let txHasValidAsset = h.txHasValidAsset(tx)             || makeError('ASSET_INVALID')
    let txIsIncoming = !txFromBridge(tx)                     || makeError('ADDRESS_BAD')

    return validPayload = await Promise.all([
        txIsValid,
        txHasMemo,
        txHasValidMemo,
        txHasMinAsset,
        txHasValidAsset,
        txIsIncoming
        ])
} 


/**
* Takes a parsed tx and validates against defined logic
* @param   {Object}   tx    Parsed transaction object
* @return  {Promise}        Promise
*/
const validateJob = async(tx) => {
    let txMemoHasArgs = h.txMemoHasArgs(tx)         || makeError('MEMO_BAD')
    let outAddressValid = h.outAddressValid(tx)        || makeError('ADDRESS_BAD')
    let jobIsNew = h.jobIsNew(tx)                   || makeError('JOB_DUPLICATE')
    
    return validJob = await Promise.all([
      txMemoHasArgs,
      outAddressValid,
      jobIsNew
    ])
} 

/* Find job helper */
const findJob = (jobNo) => {
  let job = app.jobs.filter(x => x.id == jobNo)
  return job[0] ? job[0] : false;
}

/* Find job helper */
const findJobBacklog = (jobNo) => {
  let job = app.backlog.filter(x => x.id == jobNo)
  return job[0] ? job[0] : false;
}

/* Find job index helper */
const findIndex = (jobid) => {
  let index = app.jobs.findIndex(x => x.id == jobid)
  return index < 0 ? false : index;
}

/* Validation helpers */
const h = {
  txIsTransferStream: (payload) => {
    return payload.stream
  ? payload.stream == 'transfers'
    ? true
    : false
  : payload.txHash // because the binance API has no stream. 
    ? true
    : false
  },

  txIsIncoming: (payload) => {
    return payload.data
  ? payload.data.t[0].o == app.BNB_ADDRESS
    ? true
    : false
  : payload.toTo == app.BNB_ADDRESS
    ? true
    : false
  },

  txHasValidMemo:     (tx) => {
    let verbs = ['SWITCH','CANCEL']
    return verbs.includes(tx.txMemo.command) ? true : false
  },

  txIsValid:          (tx) => { return tx && tx.txHash ? true : false },
  txHasMemo:          (tx) => { return tx && tx.txMemo ?  true :  false },
  txMemoHasArgs:      (tx) => { return tx.txMemo.args && tx.txMemo.args.length == 1 ? true : false },
  txHasValidAsset:    (tx) => { return tx.txTkn == app.BNB_ASSET ? true : false },
  txHasMinAsset:      (tx) => { return tx.txQty > 1 ? true : false},
  outAddressValid:       (tx) => { return true }, //binance.checkAddress(tx.txMemo.args[1])},
  jobIsNew:        (tx) => { return findJob(tx.txMemo.txHash) ? false : true },
  jobIsValid:      (tx) => { return findJob(tx.txMemo.txHash) ? true : false },
  jobIsLive:       (tx) => { return findJob(tx.txMemo.txHash).status == 'ACTIVE' ||  findJob(tx.txMemo.args[0]).status == 'PENDING' ? true : false },
  serviceIsLive:      () =>   { return app.state ? true : false }
}

const syncing = () =>{
  return app.state ? false : true 
}

const txFromBridge = (tx) => {
  return tx.txFrom == app.BNB_ADDRESS ? true : false;
}


/* EXPORTS */
exports.winston = winston;
exports.splitMsg = splitMsg;
exports.logger = logger;
exports.parseTransaction = parseTransaction;
exports.h = h;
exports.validatePayload = validatePayload;
exports.validateJob = validateJob;
exports.getPeriods = getPeriods;
exports.getSyncStats = getSyncStats;
exports.findJob = findJob;
exports.findJobBacklog = findJobBacklog;
exports.findIndex = findIndex;
exports.updateRateLimits = updateRateLimits;
exports.rateLimitCheck = rateLimitCheck;
exports.makeError = makeError;
exports.stall = stall
exports.syncing = syncing
exports.countJobs = countJobs;
exports.txFromBridge = txFromBridge