const express = require('express')
const utils = require('./utils')
const api = express();
const app = require('../config/config').app
const cors = require('cors')

api.use(cors())

api.get('/', (req,res) =>{
    let endpoints = {
        "all jobs": '/jobs',
        "active": '/jobs/active',
        "pending": '/jobs/pending',
        "completed": '/jobs/completed',
        "single Job": '/job/{jobNo}',
        "address": '/address/{address}'
        }
    res.send(endpoints)
})

api.get('/jobs', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.active,
            pending:  stats.pending,
            completed:   stats.completed
        },
        jobs:app.jobs
    }
    res.send(jobs);
})

api.get('/jobs/active', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.active,
            pending:  stats.pending,
            completed:   stats.completed
        },
        active: app.jobs.filter(x => x.status == 'ACTIVE')
    }
    res.send(jobs);
})

api.get('/jobs/pending', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.active,
            pending:  stats.pending,
            completed:   stats.completed
        },
        pending: app.jobs.filter(x => x.status == 'PENDING')
    }
    res.send(jobs);
})

api.get('/jobs/completed', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.active,
            pending:  stats.pending,
            completed:   stats.completed
        },
        completed: app.jobs.filter(x => x.status == 'COMPLETED')
    }
    res.send(jobs);
})

api.get('/job/:jobNo', async(req,res) =>{
    let job = app.jobs.filter(x => x.id == req.params.jobNo);
    if(job.length > 0){
        res.send(job[0]);
    } else {
        res.sendStatus(404)
    }
})

api.get('/address/:address', async(req,res) =>{
    let from = app.jobs.filter(x =>  x.in.from == req.params.address);
    let to = app.jobs.filter(x =>  x.out.to == req.params.address);
    let jobs = from.concat(to);

    if(jobs.length > 0){
        res.send({
            "address": req.params.address,
            "stats":{
                "in": from.length,
                "out": to.length,
                "total": jobs.length
            },
            "jobs":jobs
        });
    } else {
        res.sendStatus(404)
    }
})

api.listen(app.EXPRESS_PORT)