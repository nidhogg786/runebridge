/* Local Imports */
const app = require('../config/config').app;
const utils = require('./utils');
const binance = require('./binance');
const h = utils.h;

/**
 * Processes the payload received from the Binance websocket
 * @param  {Object} payload     The websocket message payload
 * Refer https://docs.binance.org/api-reference/dex-api/ws-streams.html#3-transfer
 */
const handlePayload = async(payload) => {
    utils.winston.info(`[DEBUG] READING PAYLOAD`)

    var commands = {
        "SWITCH":       newRequest,          // Incoming
    }

    try{
        await utils.validatePayload(payload)
        let tx = utils.parseTransaction(payload)
        return await commands[tx.txMemo.command](tx);
    } catch(error){
        utils.logger(error);
        return error
    }
}

/* New Request */
const newRequest = async(tx) => {
    utils.winston.info(`[DEBUG] NEW REQUEST`)
    try {
        await utils.validateJob(tx)
        app.jobs.push(new Job(tx))
        utils.logger('JOB_SUCCESS',tx.txHash);
        return app.jobs[utils.findIndex(tx.txHash)]
    } catch (error) {
        utils.logger(error, tx.txHash);
        return error;
    }   
}

/* Job8 Class */
var Job = function(tx) {
    let {txFrom, txMemo, txHash, txQty } = tx;
    let time = Date.now();

    this.id =     txHash,
    this.status = 'ACTIVE',
    this.in = {
        chain: 'BNB',
        block:  500,
        amount:  txQty,
        from:  txFrom,
        time:   Date.now(),
        hash:   txHash,
    },


    this.out = {
        chain:  'ETH',
        block: null,
        amount: txQty - 1,
        to:  tx.txMemo.args[0],
        time:   null,
        hash:   null,
    }
}


/* Release */
Job.prototype.release = function(tx){
    let {txFrom, txMemo, txHash, txQty, txValue } = tx;
    let time = Date.now();

    this.status = 'RELEASING'
    this.out.time = time;
    this.out.hash = txHash;
}

const pickOneFromBacklog = async() => {
    let thisPayload = app.backlog.shift();
    if(thisPayload){
        return await handlePayload(thisPayload, false)
    }
}

/* Poll backlog for transactions to process */
const pollBacklog = async() =>{
    setInterval(async()=> {
        if(h.serviceIsLive()){ pickOneFromBacklog() } //abstracted for testability
    }, 5000);
}

/* Load transactions and throw them on the backlog */
const loadTransactions = async() => {
    var queued = 0;
    var skipped = 0;

    let transactions = await binance.getTransactions('RECEIVE');

    for (tx of transactions.data.tx){
        if(!utils.findJob(tx.txHash) && !utils.findJobBacklog(tx.txHash)){
            app.backlog.push(tx)
            queued += 1
        } else {
            skipped += 1
        }
    }
    utils.winston.info(`[DEBUG] LOADED: ${transactions.data.total} QUEUED: ${queued} SKIPPED: ${skipped}`)
    return true;
}

/* Poll transactions to process */
const pollTransactions = async() =>{
    setInterval(async()=> {
        if(!h.serviceIsLive()){ return; }
        return await loadTransactions()
    }, app.POLL_INTERVAL);
}

/* EXPORTS */
exports.handlePayload = handlePayload;
exports.pollTransactions = pollTransactions;
exports.pollBacklog = pollBacklog;
exports.loadTransactions = loadTransactions;
exports.pickOneFromBacklog = pickOneFromBacklog;