/**
 * Local Imports
 */
require('dotenv').config()

const app = require('./config/config').app
const utils = require('./app/utils');
const binance = require('./app/binance');
const bridge = require('./app/bridge');
const api = require('./app/api');

/**
 * Runs all the functions necessary to start the bot.
 */
console.log(Date.now())
const main = async() => {
    utils.winston.info("[INFO] STARTING")

    app.state = true;
    app.jobs = [];
    app.backlog = [];
    app.binanceRateLimSecond = 5;
    app.binanceRateLimMin = 60;

    await binance.binanceConnect();
    await bridge.pollTransactions();
    await bridge.pollBacklog();
}

main();
