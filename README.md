# RUNE BEP2/ETH BRIDGE - Stateless basic BEP2 <> ERC20 BRIDGE

Experimental purposes only.

### Project stack:

- Nodejs
- GitLab CI
- Heroku

### Prerequisites

```
yarn
node v8^
```

### Env variables

Create `.env` file and set the following variables.

```
BNB_APIURI=https://dex.binance.org/ or https://testnet-dex.binance.org/ 
BNB_CHAIN=mainnet or testnet
BNB_ADDRESS= address for the bridge (binance chain)
BNB_PRIVKEY= private key for the bridge (binance chain)
BNB_ASSET= asset for the bridge
PORT=3000 or any port for the api service
```

### Project Setup

```
yarn install
yarn test
yarn start
```

## CI/CD

GitLab CI

- Deploy

Main Branch:

- master
