const config = {
    BNB_APIURI:     process.env.BNB_APIURI,
    BNB_ADDRESS:    process.env.BNB_ADDRESS,
    BNB_PRIVKEY:    process.env.BNB_PRIVKEY,
    BNB_CHAIN:      process.env.BNB_CHAIN,
    EXPRESS_PORT:   process.env.PORT,
    BNB_ASSET:      process.env.BNB_ASSET,
    POLL_INTERVAL:  process.env.POLL_INTERVAL
}

exports.app = config;