const app = require('../config/config').app
const binance = require('../app/binance')
const bridge = require('../app/bridge')
const utils = require('../app/utils')
const fs = require('fs')

const BnbApiClient = require('@binance-chain/javascript-sdk').BncClient;
jest.mock('@binance-chain/javascript-sdk');

app.jobs = []
app.state = true;
app.BNB_ADDRESS = 'BNBBRIDGE'
app.BNB_ASSET = 'RUNE-B1A'

const testdata = JSON.parse(fs.readFileSync('./__mockData__/bridge.json'))
jest.setTimeout(10000);

const mockSyncing = jest.spyOn(utils, "syncing");
mockSyncing.mockImplementation(() =>{return false});

const mockLogging = jest.spyOn(utils, "logger");
mockLogging.mockImplementation(() =>{return true});

const mockTransactions = jest.spyOn(binance, "getTransactions");
mockTransactions.mockImplementation((side) =>{
  return testdata.inputs.multi;
});

describe('SMOKE TESTS', () => {
  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  beforeEach(async() => {
    app.jobs = [];
    app.backlog = [];
    app.state = true;
  })

  test('single swap request is logged', async() => {
    await bridge.handlePayload(testdata.inputs.single)
    expect(app.jobs[0]).toMatchObject(testdata.outputs.single[0]);
  }); 

  test('transaction history pushed to the backlog', async() => {
    await bridge.loadTransactions()
    expect(app.backlog).toMatchObject(testdata.inputs.multi.data.tx);
    expect(app.backlog).toHaveLength(3)
  }); 

  test('the backlog is consumed', async() => {
    await bridge.loadTransactions()
    await bridge.pickOneFromBacklog()
    await bridge.pickOneFromBacklog()
    await bridge.pickOneFromBacklog()
    expect(app.jobs).toMatchObject(testdata.outputs.multi);
    expect(app.jobs).toHaveLength(3)
  }); 

  
  test('the backlog skips existing jobs', async() => {
    app.jobs = testdata.outputs.single

    await bridge.loadTransactions()
    await bridge.pickOneFromBacklog()
    await bridge.pickOneFromBacklog()

    expect(app.jobs).toMatchObject(testdata.outputs.multi);
    expect(app.jobs).toHaveLength(3)
  }); 

  
  test('incoming unknown asset should error', async() => {
    let result = await bridge.handlePayload(testdata.inputs.badasset);
    expect(result).toBeInstanceOf(Error)
    expect(result.message).toBe('ASSET_INVALID')
    expect(app.jobs).toHaveLength(0)
  }); 

  test('< min amount should error', async() => {
    let result = await bridge.handlePayload(testdata.inputs.badamt);
    expect(result).toBeInstanceOf(Error)
    expect(result.message).toBe('ASSET_INSUFFICIENT')
    expect(app.jobs).toHaveLength(0)
  }); 

  test('transaction from the bridge should error', async() => {
    let result = await bridge.handlePayload(testdata.inputs.frombridge);
    expect(result).toBeInstanceOf(Error)
    expect(result.message).toBe('ADDRESS_BAD')
    expect(app.jobs).toHaveLength(0)
  }); 

});